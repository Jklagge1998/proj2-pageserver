from flask import Flask, request, render_template
import os
import config

options  = config.configuration()
DOCROOT = options.DOCROOT

app = Flask(__name__)

@app.route("/")
def hello():
	return "UOCIS docker demo!"

@app.route("/<path:fileName>")
def respond(fileName):
	"""Makes the proper HTTPS repsonse given fileName"""
	path = "{}/{}".format(DOCROOT, fileName)

	if request.method == "GET":
		if Forbidden(fileName):
			return render_template("403.html"), 403
		elif os.path.isfile(path) == 0:
			return render_template("404.html"), 404
		contents = GetFileContents(path)
		return contents, 200
	else:
		return render_template("NotImplemented.html")

def Forbidden(fileName):
	""" Takes a string that represents the name of a file. It then
	checks to see if the file name contains and illegal character
	or characters and if it is a html or css file. If it has an illegal
	character or isn't a css or html file it returns true (1)"""
	symbols = 0
	filetype = 0
	if "~" in fileName or "//" in fileName or ".." in fileName:
		symbols = 1
	elif fileName.split(".")[-1] != "html" and fileName.split(".")[-1] != "css":
		filetype = 1
	return symbols or filetype

def GetFileContents(filePath):
	"""It reads and returns the contents of the file in filePath"""
	with open(filePath, "r") as file:
		fileContents = file.read()
		file.close()
	return fileContents


if __name__ == "__main__":
	app.run(debug=True,host='0.0.0.0')
