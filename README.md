A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/


# Project Overview

* Author: Jackson Klagge

* Email: Jklagge@uoregon.edu

  
* Project Overview: Created a basic python Flask webpage that runs the same functionalities as project1 which was a server made from scratch. The webpage will load the proper html and css files from the programs DOCROOT which is ./templates by default. It will also load up it's own pages for the 403 forbidden and 404 not found errors.

* Source code for this project is at: https://bitbucket.org/UOCIS322/proj2-pageserver
* Test scripts and a few files were carried over from project 1 which can be found at: https://bitbucket.org/UOCIS322/proj1-pageserver
